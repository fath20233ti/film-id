<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Film;
use App\Cast;
use App\Peran;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peran = DB::table('peran')->get();
        return view('peran.index', compact('peran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $film = Film::all();
        $cast = Cast::all();

        return view('peran.create', compact('film', 'cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'film_id' => 'required',
                'cast_id' => 'required',
                'nama' => 'required',
            ],
            [
                'film_id.required' => 'Inputan film harus diisi',
                'cast_id.required' => 'Inputan cast harus diisi',
                'nama.required' => 'Inputan nama harus diisi',
            ]
        );

        DB::table('peran')->insert(
            [
                'film_id' => $request['film_id'],
                'cast_id' => $request['cast_id'],
                'nama' => $request['nama']
            ]
        );

        // $peran = new Peran;
        // $peran->film_id = $request->film_id;
        // $peran->cast_id = $request->cast_id;
        // $peran->nama = $request->nama;
        // $peran->save();

        return redirect('/peran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = DB::table('peran')->where('id', $id)->first();
        //$peran = Peran::findOrFail($id);

        return view('peran.show', compact('peran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran = DB::table('peran')->where('id', $id)->first();
        // $peran = Peran::findOrFail($id);
        $cast = Cast::all();
        $film = Film::all();

        return view('peran.edit', compact('peran', 'cast', 'film'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'film_id' => 'required',
                'cast_id' => 'required',
                'nama' => 'required',
            ],
            [
                'film_id.required' => 'Inputan film harus diisi',
                'cast_id.required' => 'Inputan cast harus diisi',
                'nama.required' => 'Inputan nama harus diisi',
            ]
        );

        // $film = Film::find($id);

        // $peran->film_id = $request->film_id;
        // $peran->cast_id = $request->cast_id;
        // $peran->nama = $request->nama;
        // $peran->save();

        DB::table('peran')->where('id', $id)
            ->update(
                [
                    'film_id' => $request['film_id'],
                    'cast_id' => $request['cast_id'],
                    'nama' => $request['nama']
                ]
            );

        return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $peran = Peran::find($id);
        // $peran->delete();
        DB::table('peran')->where('id', '=', $id)->delete();

        return redirect('/peran');
    }
}
