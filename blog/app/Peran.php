<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = 'peran';
    // protected $fillable = ["film_id", "cast_id", "nama"];
    protected $guarded = [];

    public function casts() {
        return $this->belongsToMany('App\Cast', 'peran', 'film_id', 'cast_id');
    }
}
