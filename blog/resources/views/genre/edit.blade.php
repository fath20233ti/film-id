@extends('layout.master')
@section('judul')
    Halaman Form Edit Genre
@endsection

@section('content')
    <form action="/genre/{{$genre->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama Genre</label>
            <input type="text" value="{{$genre->nama}}" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <a href="/genre" class="btn btn-info">Cancel</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection