@extends('layout.master')
@section('judul')
  Halaman Form Create Genre
@endsection

@section('content')
    <div class="text-center">
        <img src="https://media.discordapp.net/attachments/894919708938223657/965155054514479114/IMG-20220417-WA0015.jpg" class="img-fluid" alt="Responsive image" width="40%">
    </div>
    <form action="/genre" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Genre</label>
            <input type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <a href="/genre" class="btn btn-info">Cancel</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection