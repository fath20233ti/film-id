@extends('layout.master')
@section('judul')
    Buat Account Baru
@endsection
@section('content')
    <h3>Sign Up Form</h3>
    
    <form action="/welcome" method="post" accept-charset="utf-8">
        @csrf
        <label>First name :</label><br />
        <input type="text" name="fname" /><br /><br />
        <label>Last name :</label><br />
        <input type="text" name="lname" /><br /><br />
        <label>Gender :</label><br />
        <input type="radio" name="gender" value="1" />Man <br />
        <input type="radio" name="gender" value="2" />Women <br />
        <input type="radio" name="gender" value="3" />Other <br /><br />
        <label>Nationality : </label>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singapure</option>
            <option value="3">Malaysia</option>
            <option value="4">Thailand</option>
        </select><br /><br />
        <label>Language Spoken :</label><br />
        <input type="checkbox" name="language" />Bahasa Indonesia <br />
        <input type="checkbox" name="language" />English <br />
        <input type="checkbox" name="language" />Arabic <br />
        <input type="checkbox" name="language" />Japanese <br /><br />
        <label>Bio :</label><br />
        <textarea name="bio" rows="10" cols="30"></textarea><br /><br />
        <input type="submit" value="Sign Up"/>
    </form>
@endsection