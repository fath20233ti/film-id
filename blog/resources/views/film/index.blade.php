@extends('layout.master')
@section('judul')
    Halaman List Film
@endsection

@section('content')

    <a href="/film/create" class="btn btn-primary my-3"> Add Film </a>
    <div class="row">
        @forelse ($film as $item)
            <div class="col-3">
                <div class="card">
                    <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top" alt="gambar">
                    <div class="card-body">
                      <h3>{{$item->judul}}</h3>
                      <p class="card-text">{{ Str::limit($item->ringkasan, 30) }}</p>
                      <form action="/film/{{$item->id}}" method="POST">
                        @method('delete')
                        @csrf
                        <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                      </form>
                    </div>
                </div>
            </div>
        @empty
            <h4>Data Kosong</h4>
        @endforelse
    </div>

@endsection