@extends('layout.master')
@section('judul')
    Halaman Detail Film
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="col-4">
                <img src="{{asset('poster/' . $film->poster)}}" class="card-img-top" alt="gambar">
            </div>
            <h3>{{$film->judul}}</h3>
            <p class="card-text">{{$film->ringkasan}}</p>
            <a href="/film" class="btn btn-info btn-sm">Back</a>
        </div>
    </div>

@endsection