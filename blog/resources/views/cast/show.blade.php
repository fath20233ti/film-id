@extends('layout.master')
@section('judul')
    Halaman Detail Cast
@endsection

@section('content')
    <h6>Nama : {{$cast->nama}}</h6>
    <h6>Umur : {{$cast->umur}}</h6>
    <p>Bio : {{$cast->bio}}</p>
    <a href="/cast" class="btn btn-info">Back</a>
@endsection