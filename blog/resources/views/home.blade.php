@extends('layout.master')
@section('judul')
    Sanberbook
@endsection
@section('content')
    <div>
        <h3>Sosial Media Developer Santai Berkualitas</h3>
        <p>
        Belajar dan Berbagi agar hidup ini semakin santai berkualitas
        </p>
    </div>
    <div>
        <h3>Benefit Join Sanberbook</h3>
        <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing Knowledge dari para mentor Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
    </div>
    <div>
        <h3>Cara Bergabung ke Sanberbook</h3>
        <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
        </ol>
    </div>
@endsection
    
