@extends('layout.master')
@section('judul')
    Halaman List Peran
@endsection

@section('content')

    <a href="/peran/create" class="btn btn-primary my-3"> Add Peran </a>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Film</th>
                <th scope="col">Cast</th>
                <th scope="col">Peran</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($peran as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->film_id}}</td>
                    <td>{{$item->cast_id}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/peran/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/peran/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/peran/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <h1>Data Kosong</h1>
            @endforelse
        </tbody>
    </table>
@endsection