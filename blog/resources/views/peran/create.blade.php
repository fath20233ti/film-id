@extends('layout.master')
@section('judul')
    Halaman Form Create Peran
@endsection

@section('content')
    <form action="/peran" method="POST">
        @csrf
        <div class="form-group">
            <label>Film</label>
            <select name="film_id" id="" class="form-control">
                <option value="">-- Pilih Film --</option>
                @foreach ($film as $item)
                    <option value="{{$item->id}}">{{$item->judul}}</option>
                @endforeach
            </select>
        </div>
        @error('film_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Cast</label>
            <select name="cast_id" id="" class="form-control">
                <option value="">-- Pilih Cast --</option>
                @foreach ($cast as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        @error('cast_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Nama Peran</label>
            <input type="text" class="form-control" name="peran">
        </div>
        @error('peran')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <a href="/peran" class="btn btn-info">Cancel</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection