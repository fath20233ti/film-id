<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Membuat Route halaman table dan data-tables
Route::get('/table', function() {
    return view('halaman.table');
});
Route::get('/data-tables', function() {
    return view('halaman.data-table');
});

// Membuat Route untuk Home, Register dan Welcome
Route::get('/', 'HomeController@dasboard');
Route::get('/register', 'AuthController@pendataan');
Route::post('/welcome', 'AuthController@welcome');

// Route untuk CRUD Cast
// mengarah ke form create data
Route::get('/cast/create', 'CastController@create');

// menyimpan data ke table cast pada database
Route::post('/cast', 'CastController@store');

// menampilkan semua data
Route::get('/cast', 'CastController@index');

// detail data
Route::get('/cast/{cast_id}', 'CastController@show');

// mengarah ke form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

// update data ke table cast pada database
Route::put('/cast/{cast_id}', 'CastController@update');

// delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');

// Route untuk CRUD Genre
// mengarah ke form create data
Route::get('/genre/create', 'GenreController@create');

// menyimpan data ke table genre pada database
Route::post('/genre', 'GenreController@store');

// menampilkan semua data
Route::get('/genre', 'GenreController@index');

// detail data
Route::get('/genre/{genre_id}', 'GenreController@show');

// mengarah ke form edit data
Route::get('/genre/{genre_id}/edit', 'GenreController@edit');

// update data ke table genre pada database
Route::put('/genre/{genre_id}', 'GenreController@update');

// delete data
Route::delete('/genre/{genre_id}', 'GenreController@destroy');


Route::resource('film', 'FilmController');
Route::resource('peran', 'PeranController');